# Python Flask Sample

A simple python app with flask api that just responds with a hello for all requests to /hello...

## How to start the application 

- Python 3.7 shoud be installed on the os
- Install the requirements.txt using `pip install -r requirements.txt`
- Run the application using `python server.py`
- Connect on port 8080

