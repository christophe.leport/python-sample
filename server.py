from flask import Flask, jsonify, request, abort
app = Flask(__name__)

message = "You just deployed your sample python Flask App!"

@app.route('/hello')
def tree():
    return jsonify({
        'Important Message': message
    })


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
